import React, { useEffect } from 'react';

import { Provider } from 'react-redux';
import { PersistGate } from "redux-persist/integration/react";
import { getStore, getPersistor } from "./redux/store";

import logo from './logo.svg';
import "./assets/scss/main.scss";
import Dummy from "./dummy";
import { getAllCharacters, getCharacterById, getCharacterComics, getCharacterStories } from "./services/characters";

function App() {
  const store = getStore();
  const persistor = getPersistor(store);

  useEffect(() => {
    const fetchData = async () => {
      const { data, error } = await getCharacterStories(1011334);
      console.log('data', data);
      console.log('error', error);
    }

    fetchData();
  }, []);

  return (
    <Provider store={store}>
      <PersistGate loading={null} persistor={persistor}>
        <div className="App">
          <header className="App-header">
            <img src={logo} className="App-logo" alt="logo" />
            <p>
              Edit <code>src/App.js</code> and save to reload.
            </p>
            <Dummy />
            <a
              className="App-link"
              href="https://reactjs.org"
              target="_blank"
              rel="noopener noreferrer"
            >
              Learn React
            </a>
          </header>
        </div>
      </PersistGate>
    </Provider>
  );
}

export default App;
