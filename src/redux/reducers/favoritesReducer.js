import {
    ACTION_ADD_FAVORITE_COMICS,
    ACTION_ADD_FAVORITE_CHARACTERS,
    ACTION_ADD_FAVORITE_STORIES,
    ACTION_REMOVE_FAVORITE_COMICS,
    ACTION_REMOVE_FAVORITE_CHARACTERS,
    ACTION_REMOVE_FAVORITE_STORIES,
} from "../constants";

const initialState = {
    comics: [{ id: 1, name: "Marvel Zombies" }],
    characters: [],
    stories: []
}

const reducer = (state = initialState, { type, payload }) => {
    switch (type) {
        case ACTION_ADD_FAVORITE_COMICS:
            return { ...state, comics: [...state.comics, payload] };
        default:
            return state;
    }
}

export default reducer;